set tabstop=2
set shiftwidth=3
set smarttab
set expandtab
set softtabstop=3
set autoindent
"set number
set showmatch
"set nocompatible

set colorcolumn=80

" Turn Syntax coloring on
syntax on

" Map jj to escape
imap jj <Esc>

" Long lines are break lines
map j gj
map k gk

set ttimeoutlen=50
set vb

set t_Co=256
colorscheme molokai
let g:molokai_original = 1

" Immediately starts searching for the word
set incsearch

set list listchars=tab:\ \ ,trail:·

set scrolloff=3

set cursorline

autocmd BufWritePre <buffer> :%s/\s\+$//e
autocmd BufRead,BufNewFile *.md setlocal filetype=markdown textwidth=72

"inoremap {<Space>      {}<Left>
"inoremap {<CR>  {<CR>}<Esc>O<Tab>
"inoremap {}     {
